package com.example.core.today;

import com.example.core.today.TestFragment.JsonNewsAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Fragment2 extends Fragment{

	
	private static String _content;
    Context context = null;
    
    
    
    public static Fragment2 newInstance(String content) {
        Fragment2 fragment = new Fragment2();
        _content=content;
        return fragment;
    }
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		View view = inflater.inflate(R.layout.fragment2, container, false);
		return view;	
		}

}
