package com.example.core.today;




public class JsonNewsItem {
	private String title;
	private String link;
	private String description;
	private String pubDate;
	
	
	/**
	 * Initialize with icon and data array
	 */
	public JsonNewsItem() {
		
	}
	
	public JsonNewsItem(String a) {
		
	}

	/**
	 * Initialize with icon and strings
	 */
	public JsonNewsItem(String a, String b, String c) {
		this.title = a;
		this.link =b;
		this.description = c;
		
	}
	

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPubDate() {
		return pubDate;
	}

	public void setPubDate(String pubDate) {
		this.pubDate = pubDate;
	}

	

	/**
	 * Compare with the input object
	 * 
	 * @param other
	 * @return
	 */
	public boolean compareTo(JsonNewsItem other) {
		if (title.equals(other.getTitle())) {
			return false;
		} else if (link.equals(other.getLink())) {
			return false;
		} else if (description.equals(other.getDescription())) {
			return false;
		} else if (pubDate.equals(other.getPubDate())) {
			return false;
		}
		
		return true;
	}

}
