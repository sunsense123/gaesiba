package com.example.core.today;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;


public final class TestFragment extends Fragment implements OnRefreshListener<ListView>, OnItemClickListener{
    private static final String KEY_CONTENT = "TestFragment:Content";
	private static final int REQUEST_CODE_ANOTHER = 000;
    private String mContent = "???";
    private static String _content;
    Context context = null;
    JsonNewsAdapter adapter;
    PullToRefreshListView plv = null;
    
    
    
    public static TestFragment newInstance(String content) {
        TestFragment fragment = new TestFragment();
        _content=content;
        return fragment;
    }

    

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
            mContent = savedInstanceState.getString(KEY_CONTENT);
        }
    }

    public ArrayList<JsonNewsItem> Parsing (int max){
    	                         
    	String line;
		String page ="";
		String a="a",b="b",c="c";

		final String group = ("http://kr.core.today/json/?g=");
		ArrayList<JsonNewsItem> m_orders = new ArrayList<JsonNewsItem>();
		for(int k =0; k < max ; k++){
			page = "";
		try{
			
			
			String sample = new String(group + String.valueOf(k+1));
			URL url = new URL(sample);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
		//	BufferedInputStream buf = new BufferedInputStream(urlConnection.getInputStream());
			
			BufferedReader bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
			
			int count =0;
			while((line=bufreader.readLine())!=null){
				if(line == "group")
					count++;
				if(count >1)
					break;
				page+=line;
			}
			JSONObject test;
			JSONArray alljson = new JSONArray(page); 
			test=new JSONObject(alljson.getString(0));
			a=test.getString("title");
			b = sample;
			
			
		urlConnection.disconnect();
		}
		catch(Exception e){
			Toast.makeText(context, "Fail to parse JSON", 1000).show();
		}
		/////////////////////////////////////////////////////////////////////////////////////////////parse JSON file
		
		
		
		
		
		
		
		
		/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		
		m_orders.add(new JsonNewsItem(a,b,c));
		///////////////////////////////////////////////////////////////////////////////add NewsItem to ArrayList
		
		a = "a";
		b = "b";
		c = "c";
		}
		
		
		return m_orders;
    }
    
    public LinearLayout listmaker(PullToRefreshListView plv, Context mcontext, ViewGroup container){
		plv = (PullToRefreshListView) LayoutInflater.from(context).inflate(
		R.layout.layout_listview_in_viewpager, container, false);
		adapter = new JsonNewsAdapter(context, R.layout.listitem , Parsing(1));
		plv.setAdapter(adapter);
		
		plv.setOnItemClickListener(this);
		plv.setOnRefreshListener(TestFragment.this); /// set Refresh listener
		
		LinearLayout layout = new LinearLayout(getActivity());
	    layout.setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
	    layout.setGravity(Gravity.CENTER);
		layout.addView(plv);
		/////////////////////////////////////////////////////////////////////////////inflate layout and listview
			return layout;
		}
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

			context = container.getContext(); // get Context
			
			
			
			
			
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////
			StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().detectDiskReads().detectDiskWrites().detectNetwork().penaltyLog().build());
			
			//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
				
			return listmaker(plv, context, container);
		

    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////
    public class JsonNewsAdapter extends ArrayAdapter<JsonNewsItem> {
		private ArrayList<JsonNewsItem> items;
		
		public JsonNewsAdapter(Context context, int textViewResourceId, ArrayList<JsonNewsItem> items) {
			super(context, textViewResourceId, items);
			this.items = items;
		}
	    
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {    
			View v = convertView;     
			if (v == null) {      
				LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);     
	                	v = vi.inflate(R.layout.listitem, null);          
			} 
			JsonNewsItem p = items.get(position);     
	        	if (p != null) {                 
			TextView tt = (TextView) v.findViewById(R.id.dataItem01);        
			TextView bt = (TextView) v.findViewById(R.id.dataItem02);
			TextView ct = (TextView) v.findViewById(R.id.dataItem03);
			
			if (tt != null){                 
				tt.setText(p.getTitle());                                   
			}
			if(bt != null){                  
				bt.setText(p.getLink());             
			}
			if(ct != null){                  
				ct.setText(p.getDescription());      
			} 
		}            
		return v;   
		} 
	}
    ///////////////////////////////////////////////////////////////////////listview adapter
    
    
    
    
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////
    @Override
	public void onRefresh(PullToRefreshBase<ListView> refreshView) {
		new GetDataTask(refreshView).execute();
		
	}

	private static class GetDataTask extends AsyncTask<Void, Void, Void> {

		PullToRefreshBase<?> mRefreshedView;

		public GetDataTask(PullToRefreshBase<?> refreshedView) {
			mRefreshedView = refreshedView;
		}

		@Override
		protected Void doInBackground(Void... params) {
			// Simulates a background job.
			try {
				Thread.sleep(4000);
			} catch (InterruptedException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			
			
			mRefreshedView.onRefreshComplete();
			
			
			super.onPostExecute(result);
		}
	}
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_CONTENT, mContent);
    }
    ////////////////////////////////////////////////////////////the task done during refreshed






	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		JsonNewsItem curItem = (JsonNewsItem) adapter.getItem(position-1);
		String curTitle = curItem.getTitle();
		String curDesc = curItem.getDescription();
		String curPubDate = curItem.getPubDate();
		Intent intent = new Intent(context,WholeArticle.class);
		intent.putExtra("title", curTitle);
		intent.putExtra("desc", curDesc);
		intent.putExtra("pubdate", curPubDate);
		startActivityForResult(intent,REQUEST_CODE_ANOTHER);
	}
    
    
    
}
