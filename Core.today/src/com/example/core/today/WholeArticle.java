package com.example.core.today;



import android.app.Activity;
import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

public class WholeArticle extends Activity{

	////////////////////////////////////////////////////////////////////////////////////
	public void finish() {
		super.finish();
		this.overridePendingTransition(R.anim.animation_enter2, R.anim.animation_leave2);
	}
	/////////////////////////////////////////////////////////////////////for animation leave
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		this.overridePendingTransition(R.anim.animation_enter, R.anim.animation_leave);//animation enter
	    requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.whole_article);
		
		////////////////////////////////////////////////////////////
		Intent intent = new Intent(this.getIntent());
		String curTitle,curDesc;
		curTitle = intent.getStringExtra("title");
		curDesc = intent.getStringExtra("desc");
		TextView title = (TextView) findViewById(R.id.title);
		TextView Desc = (TextView) findViewById(R.id.contents);
		title.setText(curTitle);
		Desc.setText(curDesc);
		////////////////////////////////////////////////////////////
	
	}

}
